import http from './httpServices';
import config from '../config.json';

export default async function getGenres() {
  return await http.get(config.apiUrl + '/genres');
}
