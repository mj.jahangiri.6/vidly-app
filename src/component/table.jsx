import React, { Component } from 'react';
import TableBody from './tableBody';
import TableHeader from './tableHeader';

export default class Table extends Component {
  render() {
    const { movies, columns, onSort, sortedColumn } = this.props;
    return (
      <>
        <table className="table">
          <TableHeader
            columns={columns}
            onSort={onSort}
            sortedColumn={sortedColumn}
          />
          <TableBody data={movies} columns={columns} />
        </table>
      </>
    );
  }
}
