import React from 'react';
import { useParams, useLocation, useNavigate } from 'react-router-dom';

export default function ParamsFinder() {
  const params = useParams();
  const location = useLocation();
  const navigate = useNavigate();
  return params, location, navigate;
}
