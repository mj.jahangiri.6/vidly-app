import React from 'react';

export default function Select({ name, error, label, options, ...rest }) {
  return (
    <div className="mb-3">
      <label className="form-label" htmlFor={name}>
        {label}
      </label>
      <select
        className="form-select"
        name={name}
        id={name}
        {...rest}
        aria-label="Default select example"
      >
        <option>Choose ...</option>
        {options.map((option) => (
          <option key={option._id} value={option._id}>
            {option.name}
          </option>
        ))}
      </select>
      {error && <div className="alert alert-danger small py-1">{error}</div>}
    </div>
  );
}
