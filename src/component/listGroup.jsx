import React from 'react';

export default function listGroup({
  items,
  textProperty,
  valueProperty,
  selectedItem,
  onItemSelect,
}) {
  return (
    <div>
      <ul className="list-group">
        {items.map((item) => (
          <li
            key={item[valueProperty]}
            onClick={() => onItemSelect(item[textProperty])}
            className={
              item[textProperty] === selectedItem
                ? 'list-group-item active'
                : 'list-group-item'
            }
          >
            {item[textProperty]}
          </li>
        ))}
      </ul>
    </div>
  );
}

listGroup.defaultProps = {
  textProperty: 'name',
  valueProperty: '_id',
};
