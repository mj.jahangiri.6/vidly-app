import React, { Component } from 'react';
import Form from './form';
import Joi from 'joi-browser';
import { getMovie, saveMovie } from '../Services/movieServices';
import getGenres from '../Services/genreServices';

export default class MoviesForm extends Form {
  state = {
    data: {
      // _id: '',
      title: '',
      genreId: '',
      numberInStock: '',
      dailyRentalRate: '',
    },
    genres: [],
    errors: {},
  };
  schema = {
    _id: Joi.string(),
    title: Joi.string().required().label('Title'),
    genreId: Joi.string().required().label('Genre'),
    numberInStock: Joi.number()
      .integer()
      .required()
      .min(0)
      .max(100)
      .label('Number in Stock'),
    dailyRentalRate: Joi.number().required().min(0).max(10).label('Rate'),
  };

  async componentDidMount() {
    const movieId = this.props.id;
    const { data: genres } = await getGenres();
    this.setState({ genres });
    if (movieId === 'new') return;
    const { data: movie } = await getMovie(movieId);
    if (movie === undefined) return this.props.navigate('/not-found', true);
    this.setState({ data: this.mapToViewModel(movie) });
  }

  mapToViewModel(movie) {
    return {
      _id: movie._id,
      title: movie.title,
      genreId: movie.genre._id,
      numberInStock: movie.numberInStock,
      dailyRentalRate: movie.dailyRentalRate,
    };
  }

  doSubmit = async () => {
    await saveMovie(this.state.data);
    this.props.navigate('/movies', true);
  };

  render() {
    return (
      <div className="container my-4">
        <h2>Movie Form</h2>
        <form onSubmit={this.handleSubmit} className="w-75 my-3">
          {this.renderInput('title', 'Title', 'text')}
          {this.renderSelect('genreId', 'Genre', this.state.genres)}
          {this.renderInput('numberInStock', 'Number in Stock', 'number')}
          {this.renderInput('dailyRentalRate', 'Rate', 'number')}
          {this.renderButton('Save')}
        </form>
      </div>
    );
  }
}
