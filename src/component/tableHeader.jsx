import React, { Component } from 'react';
import { TiArrowSortedDown, TiArrowSortedUp } from 'react-icons/ti';

export default class MoviesTableHeader extends Component {
  raiseSort = (path) => {
    const sortedColumn = { ...this.props.sortedColumn };
    if (sortedColumn.path === path)
      sortedColumn.order = sortedColumn.order === 'asc' ? 'desc' : 'asc';
    else {
      sortedColumn.path = path;
      sortedColumn.order = 'asc';
    }
    this.props.onSort(sortedColumn);
  };

  renderSortIcon = (column) => {
    if (column.path !== this.props.sortedColumn.path) return null;
    if (this.props.sortedColumn.order === 'asc') return <TiArrowSortedDown />;
    if (this.props.sortedColumn.order === 'desc') return <TiArrowSortedUp />;
  };

  render() {
    return (
      <thead>
        <tr>
          {this.props.columns.map((column) => (
            <th
              style={{ cursor: 'pointer' }}
              key={column.path || column.key}
              onClick={() => this.raiseSort(column.path)}
            >
              {column.label} {this.renderSortIcon(column)}
            </th>
          ))}
        </tr>
      </thead>
    );
  }
}
