import React from 'react';

export default function header(props) {
  return (
    <div>
      {props.count === 0 ? (
        <h4>There are no movie in database</h4>
      ) : (
        <h4>Showing {props.count} movies in Database</h4>
      )}
    </div>
  );
}
