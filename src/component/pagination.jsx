import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import _ from 'lodash';

export default function pagination({
  currentPage,
  pageSize,
  itemCount,
  onPageChange,
}) {
  const pageCount = Math.ceil(itemCount / pageSize);

  if (pageCount === 1) return null;

  const pages = _.range(1, pageCount + 1);

  return (
    <div>
      <ul className="pagination">
        {pages.map((page) => (
          <li
            key={page}
            style={{ cursor: 'pointer' }}
            className={page === currentPage ? 'page-item active' : 'page-item'}
          >
            <button onClick={() => onPageChange(page)} className="page-link">
              {page}
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
}
