import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Table from './table';
import Like from './like';

export default class MoviesTable extends Component {
  columns = [
    {
      path: 'title',
      label: 'Title',
      content: (movie) => <Link to={`/movies/${movie._id}`}>{movie.title}</Link>,
    },
    { path: 'genre.name', label: 'Genre' },
    { path: 'numberInStock', label: 'Stock' },
    { path: 'dailyRentalRate', label: 'Rate' },
    {
      key: 'Like',
      content: (movie) => <Like movie={movie} onLike={this.props.onLike} />,
    },
    {
      key: 'Delete',
      content: (movie) => (
        <button
          className="btn btn-danger btn-sm"
          onClick={() => this.props.onDelete(movie)}
        >
          Delete
        </button>
      ),
    },
  ];
  render() {
    const { movies, onDelete, onSort, sortedColumn } = this.props;
    return (
      <Table
        columns={this.columns}
        movies={movies}
        onDelete={onDelete}
        onSort={onSort}
        sortedColumn={sortedColumn}
      />
    );
  }
}
