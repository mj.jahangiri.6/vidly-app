import React from 'react';

export default function Input({ name, label, error, ...rest }) {
  return (
    <div className="mb-3">
      <label className="form-label" htmlFor={name}>
        {label}
      </label>
      <input className="form-control" {...rest} id={name} name={name} />
      {error && <div className="alert alert-danger small py-1">{error}</div>}
    </div>
  );
}
