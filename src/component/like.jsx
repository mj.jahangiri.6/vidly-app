import React from 'react';
import { BsHeart } from 'react-icons/bs';
import { BsHeartFill } from 'react-icons/bs';

export default function Like(props) {
  return (
    <button
      className="btn bg-transparent"
      onClick={() => props.onLike(props.movie)}
    >
      {props.movie.liked === true ? <BsHeartFill /> : <BsHeart />}
    </button>
  );
}
