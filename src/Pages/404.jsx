import React from 'react';

export default function NotFound() {
  return (
    <div>
      <h1 className="text-center bg-warning text-black p-5">Page Not Found</h1>
    </div>
  );
}
