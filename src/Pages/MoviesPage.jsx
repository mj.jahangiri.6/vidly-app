import React from 'react';
import MoviesForm from '../component/MoviesForm';
import { useLocation, useNavigate } from 'react-router-dom';

export default function MoviesPage() {
  const location = useLocation();
  const navigate = useNavigate();

  if (location.pathname === '/movies/new')
    return <MoviesForm id={'new'} navigate={navigate} />;
  else if (location.pathname !== '/movies/new')
    return (
      <MoviesForm id={location.pathname.split('/')[2]} navigate={navigate} />
    );
}
