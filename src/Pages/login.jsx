import React, { Component } from 'react';
import Form from '../component/form';
import Joi from 'joi-browser';

export default class login extends Form {
  state = {
    data: { username: '', password: '' },
    errors: {},
  };

  schema = {
    username: Joi.string().required().label('Username'),
    password: Joi.string().required().label('Password'),
  };

  doSubmit = () => {
    console.log('Submited');
  };

  render() {
    return (
      <div className="my-4 container mx-auto">
        <h2>Login</h2>
        <form onSubmit={this.handleSubmit} className="w-25 my-5">
          {this.renderInput('username', 'Username', '', true)}
          {this.renderInput('password', 'Password', 'password')}
          <div className="mb-3">{this.renderButton('Login')}</div>
        </form>
      </div>
    );
  }
}
