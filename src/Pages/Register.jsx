import React, { Component } from 'react';
import Form from '../component/form';
import Joi from 'joi-browser';

export default class Register extends Form {
  state = {
    data: { username: '', password: '', name: '' },
    errors: {},
  };

  schema = {
    username: Joi.string().required().email().label('Username'),
    password: Joi.string().required().min(5).label('Password'),
    name: Joi.string().required().label('Name'),
  };

  doSubmit = () => {
    console.log('Registered');
  };

  render() {
    return (
      <div className="my-4 container mx-auto">
        <h2>Register</h2>
        <form onSubmit={this.handleSubmit} className="w-25 my-5">
          {this.renderInput('username', 'Username', '', true)}
          {this.renderInput('password', 'Password', 'password')}
          {this.renderInput('name', 'Name')}
          <div className="mb-3">{this.renderButton('Register')}</div>
        </form>
      </div>
    );
  }
}
