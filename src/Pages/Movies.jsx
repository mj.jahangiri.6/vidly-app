import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import Pagination from '../component/pagination';
import ListGroup from '../component/listGroup';
import getMovies, { deleteMovie } from '../Services/movieServices';
import getGenres from '../Services/genreServices';
import Header from '../component/header';
import MoviesTable from '../component/moviesTable';
import _ from 'lodash';
import { paginate } from '../component/paginate';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Search from '../component/Search';
import { toast } from 'react-toastify';

export default class Movies extends Component {
  state = {
    movies: [],
    genres: [],
    pageSize: 4,
    currentPage: 1,
    selectedGenre: null,
    searchQuery: '',
    sortedColumn: { path: 'title', order: 'asc' },
  };

  async componentDidMount() {
    const { data: genresAll } = await getGenres();
    const allGenres = [{ _id: '', name: 'All Genres' }, ...genresAll];
    const { data: movies } = await getMovies();
    this.setState({
      movies,
      genres: allGenres,
    });
  }

  // Page pagination handler on pagination component
  handlerPageChange = (page) => {
    this.setState({ currentPage: page });
  };

  // Movie like handler on like button
  likeHandler = (movie) => {
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index] = { ...movies[index] };
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };

  // Movie Delete handler on delete button
  deleteHandler = async (movie) => {
    const originalMovies = this.state.movies;
    const movies = originalMovies.filter((m) => m._id !== movie._id);
    this.setState({ movies });

    try {
      await deleteMovie(movie._id);
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        toast.error('expected Error with 404 code');
      this.setState({ movies: originalMovies });
    }
  };

  // Genre filter handler on listGroup component
  genreHandler = (genre) => {
    this.setState({ selectedGenre: genre, searchQuery: '', currentPage: 1 });
  };

  searchHandle = (query) => {
    this.setState({ selectedGenre: '', searchQuery: query, currentPage: 1 });
  };

  // Sorted Filter handler on MoviesTable
  sortHandler = (sortedColumn) => {
    this.setState({ sortedColumn });
  };
  getPagedData = () => {
    const {
      pageSize,
      movies,
      currentPage,
      searchQuery,
      selectedGenre,
      sortedColumn,
    } = this.state;

    let filtred = movies;
    if (searchQuery)
      filtred = movies.filter((m) =>
        m.title.toLowerCase().startsWith(searchQuery.toLowerCase())
      );
    else if (selectedGenre)
      filtred =
        selectedGenre === '' || selectedGenre === 'All Genres'
          ? movies
          : movies.filter((m) => m.genre.name === selectedGenre);

    const sorted = _.orderBy(filtred, sortedColumn.path, sortedColumn.order);
    const allMovies = paginate(sorted, currentPage, pageSize);
    return { totalCount: filtred.length, data: allMovies };
  };

  render() {
    const {
      genres,
      pageSize,
      currentPage,
      searchQuery,
      selectedGenre,
      sortedColumn,
    } = this.state;
    const { totalCount, data: allMovies } = this.getPagedData();
    return (
      <>
        <div className="container">
          <div className="row my-4 p-0 mx-0">
            <div className="col-3">
              <ListGroup
                items={genres}
                selectedItem={selectedGenre}
                onItemSelect={this.genreHandler}
              />
            </div>
            <div className="col">
              <Link to="/movies/new" className="btn btn-primary mb-3">
                New Movie
              </Link>

              <Header count={totalCount} />
              <Search value={searchQuery} onChange={this.searchHandle} />
              <hr />
              <MoviesTable
                movies={allMovies}
                sortedColumn={sortedColumn}
                onDelete={this.deleteHandler}
                onSort={this.sortHandler}
                onLike={this.likeHandler}
              />
              <Pagination
                pageSize={pageSize}
                itemCount={totalCount}
                currentPage={currentPage}
                onPageChange={this.handlerPageChange}
              />
            </div>
          </div>
        </div>
      </>
    );
  }
}

Pagination.propTypes = {
  pageSize: propTypes.number.isRequired,
  currentPage: propTypes.number.isRequired,
  onPageChange: propTypes.func.isRequired,
};
